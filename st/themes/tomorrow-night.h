/* 8 normal colors */
static const char black[]		= "#1d1f21";
static const char red[]			= "#cc6666"; 
static const char green[] 		= "#b5bd68";
static const char yellow[]		= "#e6c547";
static const char blue[] 		= "#81a2be";
static const char magenta[]		= "#b294bb";
static const char cyan[]  		= "#70c0ba";
static const char white[] 		= "#373b41";

/* 8 bright colors */
static const char b_black[]		= "#666666";
static const char b_red[]		= "#ff3334"; 
static const char b_green[] 	= "#9ec400";
static const char b_yellow[]	= "#f0c674";
static const char b_blue[] 		= "#81a2be";
static const char b_magenta[]	= "#b77ee0";
static const char b_cyan[]  	= "#54ced6";
static const char b_white[] 	= "#282a2e";

/* special colors */
static const char background[]  = "#1d1f21";
static const char foreground[] 	= "#c5c8c6";
