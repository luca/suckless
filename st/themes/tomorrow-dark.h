/* 8 normal colors */
static const char black[]		= "#282a2e";
static const char red[]			= "#a54242"; 
static const char green[] 		= "#8c9440";
static const char yellow[]		= "#de935f";
static const char blue[] 		= "#5f819d";
static const char magenta[]		= "#85678f";
static const char cyan[]  		= "#5e8d87";
static const char white[] 		= "#707880";

/* 8 bright colors */
static const char b_black[]		= "#373b41";
static const char b_red[]		= "#cc6666"; 
static const char b_green[] 	= "#b5bd68";
static const char b_yellow[]	= "#f0c674";
static const char b_blue[] 		= "#81a2be";
static const char b_magenta[]	= "#b294bb";
static const char b_cyan[]  	= "#8abeb7";
static const char b_white[] 	= "#c5c8c6";

/* special colors */
static const char background[]  = "#1d1f21";
static const char foreground[] 	= "#c5c8c6";
