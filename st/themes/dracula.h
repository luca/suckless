/* 8 normal colors */
static const char black[]		= "#21222C";
static const char red[]			= "#ff5555"; 
static const char green[] 		= "#50fa7b";
static const char yellow[]		= "#f1fa8c";
static const char blue[] 		= "#bd93f9";
static const char magenta[]		= "#ff79c6";
static const char cyan[]  		= "#8be9fd";
static const char white[] 		= "#f8f8f2";

/* 8 bright colors */
static const char b_black[]		= "#6272a4";
static const char b_red[]		= "#ff6e6e"; 
static const char b_green[] 	= "#69ff94";
static const char b_yellow[]	= "#ffffa5";
static const char b_blue[] 		= "#d6acff";
static const char b_magenta[]	= "#ff92df";
static const char b_cyan[]  	= "#a4ffff";
static const char b_white[] 	= "#ffffff";

/* special colors */
static const char background[]  = "#21222C";
static const char foreground[] 	= "#d7d7d7";
