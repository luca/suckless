/* 8 normal colors */
static const char black[]		= "#1d1f21";
static const char red[]			= "#cc241d"; 
static const char green[] 		= "#98971a";
static const char yellow[]		= "#d7992a";
static const char blue[] 		= "#458588";
static const char magenta[]		= "#b16286";
static const char cyan[]  		= "#689d6a";
static const char white[] 		= "#c5c8c6";

/* 8 bright colors */
static const char b_black[]		= "#969896";
static const char b_red[]		= "#fb4934"; 
static const char b_green[] 	= "#b8bb26";
static const char b_yellow[]	= "#fabd2f";
static const char b_blue[] 		= "#83a598";
static const char b_magenta[]	= "#d3869b";
static const char b_cyan[]  	= "#8ec07c";
static const char b_white[] 	= "#d7d7d7";

/* special colors */
static const char background[]  = "#1d1f21";
static const char foreground[] 	= "#c5c8c6";
